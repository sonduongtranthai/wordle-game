# Simple Wordle Game
Simple Wordle Game by ReactJs and [https://mui.com](Material UI)

# Start Frontent
From the root directory

## Setup
### ENVIRONMENT

Before start project, create environment file name `.env` and add values, for example:
`.env.example` with this content:
```text
REACT_APP_API_URL=https://wordle.votee.dev:8000
```

### `npm install`

Downloads all packages and it's dependencies.

### `npm start`
Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.\
You will also see any lint errors in the console.

### `npm run test`

Launches the test runner in the interactive watch mode.\
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `npm run build`

Builds the app for production to the `build` folder.\
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.\
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.