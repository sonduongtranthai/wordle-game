import React, { ReactNode } from 'react'
import { Key, EnterButton, RemoveButton } from './Key'
import { Box } from '@mui/material';
import { CharStatus } from '../type'

type Props = {
    children?: ReactNode
    correctLetters: string[]
    absentLetters: string[]
    presentLetters: string[]
    onClick: (value: string) => void
    onClickEnter: () => void
    onClickRemove: () => void
}

export const Keyboard = ({
    correctLetters = [], absentLetters = [], presentLetters = [],
    onClick, onClickEnter, onClickRemove
}: Props) => {

    const handleClickKey = (value: string) => {
        onClick(value)
    }

    const handleClickEnter = () => {
        onClickEnter()
    }

    const handleClickRemove = () => {
        onClickRemove()
    }
    const charStatuses = (value: string): CharStatus => {
        if (correctLetters.includes(value))
            return 'correct'
        if (presentLetters.includes(value))
            return 'present'
        if (absentLetters.includes(value))
            return 'absent'
        return 'normal'
    }

    return (
        <Box>
            <Box
                sx={{
                    display: 'flex',
                    justifyContent: 'center',
                    marginBottom: 1
                }}
            >
                {['Q', 'W', 'E', 'R', 'T', 'Y', 'U', 'I', 'O', 'P'].map((key) => (
                    <Key
                        value={key}
                        key={key}
                        onClick={handleClickKey}
                        status={charStatuses(key)}
                    />
                ))}
            </Box>
            <Box
                sx={{
                    display: 'flex',
                    justifyContent: 'center',
                    marginBottom: 1
                }}
            >
                {['A', 'S', 'D', 'F', 'G', 'H', 'J', 'K', 'L'].map((key) => (
                    <Key
                        value={key}
                        key={key}
                        onClick={handleClickKey}
                        status={charStatuses(key)}
                    />
                ))}
            </Box>
            <Box
                sx={{
                    display: 'flex',
                    justifyContent: 'center',
                    marginBottom: 4
                }}
            >
                <EnterButton onClick={handleClickEnter} />
                {['Z', 'X', 'C', 'V', 'B', 'N', 'M'].map((key) => (
                    <Key
                        value={key}
                        key={key}
                        onClick={handleClickKey}
                        status={charStatuses(key)}
                    />
                ))}
                <RemoveButton onClick={handleClickRemove} />

            </Box>

        </Box>

    )
}
