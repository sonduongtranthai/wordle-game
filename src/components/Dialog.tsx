import React, { useEffect, useState } from 'react'
import {
    TextField, DialogTitle,
    DialogContentText, DialogContent,
    DialogActions, Dialog, Button,
    FormControl, InputLabel, OutlinedInput, Grid,
    InputAdornment, IconButton, MenuItem, Typography, Box
} from '@mui/material'
import { VisibilityOff, Visibility } from '@mui/icons-material'

type PropsResetDialog = {
    onCancel: () => void
    onReset: () => void
    open: boolean
}

export const ResetDialog = ({ onCancel, onReset, open }: PropsResetDialog) => {
    const handleClose = () => {
        onCancel();
    };
    const handleReset = () => {
        onReset();
    };
    return (
        <div>
            <Dialog
                open={open}
                onClose={handleClose}
                aria-labelledby="alert-dialog-title"
                aria-describedby="alert-dialog-description"
            >
                <DialogTitle id="alert-dialog-title">
                    {"Are you sure you want to play again"}
                </DialogTitle>
                <DialogContent>
                    <DialogContentText id="alert-dialog-description">
                        Your data will be reset
                    </DialogContentText>
                </DialogContent>
                <DialogActions>
                    <Button onClick={handleClose}>Cancel</Button>
                    <Button onClick={handleReset} autoFocus>Reset </Button>
                </DialogActions>
            </Dialog>
        </div>
    );
}

export type TypeSetting = {
    mode: string, maxRow?: number, correctWord?: string, sizeWord?: number
}

type PropsSettingDialog = {
    onCancel: () => void
    onChangeSetting: (setting: TypeSetting) => void
    open: boolean
    currentSetting: TypeSetting
}



export const SettingDialog = ({ onCancel, onChangeSetting, open, currentSetting }: PropsSettingDialog) => {
    const [mode, setMode] = useState<string>('daily')
    const [maxRow, setMaxRow] = useState<number>(6)
    const [sizeWord, setSizeWord] = useState<number>(5)
    const [correctWord, setCorrectWord] = useState<string>('')
    const [errorMessage, setErrorMessage] = useState<string>('')
    const [guide, setGuide] = useState<string>('')
    const [showCorrectWord, setShowCorrectWord] = useState<boolean>(false)

    const modes = [
        { value: 'daily', label: 'Daily' },
        { value: 'custom', label: 'Custom Word' },
        { value: 'random', label: 'Random Word' },
    ]

    const guides = {
        daily: 'Guess a chain of words from 4 to 11 letters every 24 hours',
        custom: 'Choose a word and challenge your friends',
        random: 'Try to guess a random word',
    }

    React.useEffect(() => {
        setMode(currentSetting.mode || 'daily')
        setCorrectWord(currentSetting.correctWord || '')
        setMaxRow(currentSetting.maxRow || 6)
        setSizeWord(currentSetting.sizeWord || 5)
    }, [currentSetting])

    useEffect(() => {
        if (mode === 'daily')
            setGuide(guides.daily)
        if (mode === 'custom')
            setGuide(guides.custom)
        if (mode === 'random')
            setGuide(guides.random)
    }, [mode])

    const handleClose = () => {
        onCancel();
    };

    const checkValidate = (): boolean => {
        if ((mode === 'daily' || mode === 'random') && sizeWord >= 4 && sizeWord <= 11) return true
        if (mode === 'custom' && (correctWord.length > 11 || correctWord.length < 4)) {
            setErrorMessage('Maximum length of word must be from 4 to 11!')
            return false
        }

        if (mode === 'daily' && (sizeWord < 4 || sizeWord > 11)) {
            setErrorMessage('Maximum size of word must be from 4 to 11!')
            return false
        }

        if (maxRow <= 0 || maxRow > 11) {
            setErrorMessage('Maximum row must be from 2 to 11!')
            return false
        }
        if (!correctWord) {
            setErrorMessage('You must enter correct word!')
            return false
        }
        if (correctWord.length < 4) {
            setErrorMessage('Word length is too short!')
            return false
        }
        return true
    }

    const handleChangeSetting = () => {
        if (checkValidate()) {
            onChangeSetting({ mode, maxRow, correctWord: correctWord.toUpperCase(), sizeWord })
            setErrorMessage('')
        }
    };

    const handleMouseDownCorrectWord = (event: React.MouseEvent<HTMLButtonElement>) => {
        event.preventDefault();
    };

    const handleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
        setErrorMessage('')
        const fieldName = event.target.name
        if (fieldName === 'mode')
            setMode(event.target.value)
        if (fieldName === 'maxRow')
            setMaxRow(Number(event.target.value))
        if (fieldName === 'correctWord')
            setCorrectWord(event.target.value)
        if (fieldName === 'sizeWord')
            setSizeWord(Number(event.target.value))
    };

    return (
        <Dialog
            open={open}
            // onClose={handleClose}
            aria-labelledby="alert-dialog-title"
            aria-describedby="alert-dialog-description"
        >
            <DialogTitle id="alert-dialog-title" style={{ fontWeight: 900 }}>
                Setting
            </DialogTitle>
            <DialogContent>
            <Grid container direction="column" alignItems="center">
                <FormControl sx={{ m: 1, width: 430 }} variant="outlined">
                    <TextField
                        id="mode"
                        name="mode"
                        select
                        label="Mode Select"
                        value={mode}
                        onChange={handleChange}
                    >
                        {modes.map((option) => (
                            <MenuItem key={option.value} value={option.value}>
                                {option.label}
                            </MenuItem>
                        ))}
                    </TextField>
                </FormControl>
                {mode === 'custom' ? (
                    <FormControl sx={{ m: 1, width: 430 }} variant="outlined">
                        <InputLabel htmlFor="outlined-adornment-correct-word">Correct Word</InputLabel>
                        <OutlinedInput
                            id="correctWord"
                            name="correctWord"
                            type={showCorrectWord ? 'text' : 'password'}
                            value={correctWord}
                            onChange={handleChange}
                            endAdornment={
                                <InputAdornment position="end">
                                    <IconButton
                                        aria-label="toggle password visibility"
                                        onClick={() => setShowCorrectWord(!showCorrectWord)}
                                        onMouseDown={handleMouseDownCorrectWord}
                                        edge="end"
                                    >
                                        {showCorrectWord ? <VisibilityOff /> : <Visibility />}
                                    </IconButton>
                                </InputAdornment>
                            }
                            label="Correct word"
                        />
                    </FormControl>
                ) : (
                    <FormControl sx={{ m: 1, width: 430 }} variant="outlined">
                        <TextField
                            id="sizeWord"
                            name="sizeWord"
                            type={'number'}
                            value={sizeWord}
                            onChange={handleChange}
                            label="Size of word"
                        />
                    </FormControl>
                )}
                <FormControl sx={{ m: 1, width: 430 }} variant="outlined">
                    <TextField
                        id="maxRow"
                        name="maxRow"
                        type={'number'}
                        value={maxRow}
                        onChange={handleChange}
                        label="Max row"
                    />
                </FormControl>
                <Typography component="div" variant="body1">
                    <Box sx={{ color: 'info.main', display: 'flex', justifyContent: 'center', paddingBottom: 4, paddingTop: 4 }}>{guide}</Box>
                </Typography>
                </Grid>
            </DialogContent>
            <DialogActions>
                <Button onClick={handleChangeSetting} variant="contained" style={{color: 'white'}}>Apply</Button>
                <Button onClick={handleClose} variant="outlined">Cancel</Button>
            </DialogActions>
            {errorMessage && <Typography component="div" variant="body1">
                <Box sx={{ color: 'error.main', display: 'flex', justifyContent: 'center', paddingBottom: 2, paddingTop: 2 }}>{errorMessage}</Box>
            </Typography>}
        </Dialog>
    );
}
