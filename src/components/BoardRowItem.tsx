import React from 'react'
import { Box } from '@mui/material'
import { grey, green, yellow } from '@mui/material/colors';

type PropsBoard = {
  value?: string,
  status?: string,
}



export const BoardRowItem = ({
  value,
  status,
}: PropsBoard) => {
  const styleOfBox = () => {
    const generalStyle = {
      width: 44,
      height: 44,
      margin: 0.5,
      fontWeight: 900,
      fontSize: 'xx-large',
      display: 'flex',
      justifyContent: 'center',
      alignItems: 'center',
      border: '2px solid',
      borderRadius: 1,
      
    }
    if (status === 'absent')
      return {
        ...generalStyle,
        backgroundColor: grey[600],
        color: grey[50],
        borderColor: grey[700]
      }
    if (status === 'present')
      return {
        ...generalStyle,
        backgroundColor: yellow[800],
        color: grey[200],
        borderColor: grey[700]
      }
    if (status === 'correct')
      return {
        ...generalStyle,
        backgroundColor: green[600],
        color: grey[200],
        borderColor: grey[700],
      }
    if (status === 'current' && value)
      return {
        ...generalStyle,
        backgroundColor: grey[50],
        color: grey[900],
        borderColor: grey[700]
      }
      if (status === 'current' && !value)
      return {
        ...generalStyle,
        backgroundColor: grey[200],
        color: grey[900],
        borderColor: grey[700]
      }
    return {
      ...generalStyle,
      color: grey[900],
      backgroundColor: 'white',
      borderColor: grey[400]
    }
  }
  return <Box sx={styleOfBox}> {value} </Box>
}