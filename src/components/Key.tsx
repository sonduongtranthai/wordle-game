import React, { ReactNode } from 'react'
import { styled } from '@mui/material/styles';
import { CharStatus } from '../type'
import { grey, green, yellow } from '@mui/material/colors';
import Button, { ButtonProps } from '@mui/material/Button';
import { IconButtonProps } from '@mui/material/IconButton';
import BackspaceIcon from '@mui/icons-material/Backspace';

const CorrectButton = styled(Button)<ButtonProps>(({ theme }) => ({
  color: theme.palette.getContrastText(green[500]),
  backgroundColor: green[500],
  fontWeight: 900,
  '&:hover': {
    backgroundColor: green[600],
    color: grey[400],
  },
  margin: 4,
  minWidth: 0
}));

const PresentButton = styled(Button)<ButtonProps>(({ theme }) => ({
  color: grey[600],
  backgroundColor: yellow[500],
  fontWeight: 900,
  '&:hover': {
    backgroundColor: yellow[600],
    color: grey[700],
  },
  margin: 4,
  minWidth: 0
}));

const AbsentButton = styled(Button)<ButtonProps>(({ theme }) => ({
  color: grey[50],
  backgroundColor: grey[500],
  fontWeight: 900,
  '&:hover': {
    backgroundColor: grey[700],
    color: grey[300],
  },
  margin: 4,
  minWidth: 0
}));

const NormalButton = styled(Button)<ButtonProps>(({ theme }) => ({
  color: grey[900],
  backgroundColor: grey[300],
  fontWeight: 900,
  '&:hover': {
    backgroundColor: grey[500],
    color: grey[300],
  },
  margin: 4,
  minWidth: 0
}));

const RemoveCustomButton = styled(Button)<IconButtonProps>(({ theme }) => ({
  color: grey[900],
  backgroundColor: grey[300],
  fontWeight: 900,
  '&:hover': {
    backgroundColor: grey[500],
    color: grey[300],
  },
  margin: 4,
}));

type Props = {
  children?: ReactNode
  value: string
  width?: number
  status?: CharStatus
  onClick: (value: string) => void
  isRevealing?: boolean
}

export const Key = ({
  status,
  value,
  onClick,
}: Props) => {
  const handleClick: React.MouseEventHandler<HTMLButtonElement> = (event) => {
    onClick(value)
    event.currentTarget.blur()
  }

  switch (status) {
    case 'correct':
      return <CorrectButton variant="contained" onClick={handleClick}>{value}</CorrectButton>
    case 'present':
      return <PresentButton variant="contained" onClick={handleClick}>{value}</PresentButton>
    case 'absent':
      return <AbsentButton variant="contained" onClick={handleClick}>{value}</AbsentButton>
    default:
      return <NormalButton variant="contained" onClick={handleClick}>{value}</NormalButton>
  }

}

type PropsEnterButton = {
  onClick: () => void
}


export const EnterButton = ({
  onClick,
}: PropsEnterButton) => {
  const handleClick = () => { onClick() }
  const styleOnMobile =
    window.screen.availWidth < 430 ?
      { fontSize: 10 }
      : {};
  return <NormalButton variant="contained" onClick={handleClick} style={styleOnMobile}>ENTER</NormalButton>
}

type PropsRemoveButton = {
  onClick: () => void
}


export const RemoveButton = ({
  onClick,
}: PropsRemoveButton) => {
  const handleClick = () => {
    onClick()
  }

  return <RemoveCustomButton variant="contained" onClick={handleClick}>
    <BackspaceIcon />
  </RemoveCustomButton>

}