import { Box } from '@mui/material'
import React from 'react'
import { BoardRowItem } from './BoardRowItem'

type PropsEmptyRow = {
  correctWord: string,
}


export const EmptyRow = ({
  correctWord
}: PropsEmptyRow) => {
  return <Box
    sx={{
      display: 'flex',
      justifyContent: 'center',
      marginBottom: 1,
    }}
  >
    {Array.from(Array(correctWord.length)).map((_, index) => (
      <BoardRowItem key={index}/>
    ))
    }
  </Box>
}


type PropsCurrentRow = {
  correctWord: string,
  word: string,
}

export const CurrentRow = ({
  word, correctWord
}: PropsCurrentRow) => {
  const characters = word ? word.split('') : []
  const getArrayFromLengthWord = Array.from(Array(correctWord.length))

  return <Box
    sx={{
      display: 'flex',
      justifyContent: 'center',
      marginBottom: 1,
    }}
  >
    {
      getArrayFromLengthWord.map((_, index) => (
        <BoardRowItem value={characters[index]} status="current" key={index}/>
      ))
    }
  </Box>
}


type PropsCompleteRow = {
  correctWord: string,
  word: string,
  presentLetters: string[]
}

export const CompleteRow = ({
  word, correctWord, presentLetters
}: PropsCompleteRow) => {
  const characters = word.split('')
  const correctCharacters = correctWord.toUpperCase().split('')
  const getArrayFromLengthWord = Array.from(Array(characters.length))
  const getStatus = (character: string, index: number) => {
    if (character === correctCharacters[index])
      return 'correct'
    if (correctCharacters.includes(character) || presentLetters.includes(character))
      return 'present'
    return 'absent'
  }

  return <Box
    sx={{
      display: 'flex',
      justifyContent: 'center',
      marginBottom: 1,
    }}
  >
    {
      getArrayFromLengthWord.map((_, index) => (
        <BoardRowItem value={characters[index]} status={getStatus(characters[index], index)} key={index}/>
      ))
    }
  </Box>
}