import React from 'react'
import { DialogTitle, DialogActions, Dialog, Button, DialogContent, DialogContentText } from '@mui/material'
import { green, red } from '@mui/material/colors';


type PropsWonDialog = {
    onOK: () => void
    open: boolean
}

export const WonDialog = ({ onOK, open }: PropsWonDialog) => {
    const handleClose = () => {
        onOK();
    };

    return (
        <div>
            <Dialog
                open={open}
                onClose={handleClose}
                aria-labelledby="alert-dialog-title"
                aria-describedby="alert-dialog-description"
            >
                <DialogTitle id="alert-dialog-title" style={{
                    color: green[600], fontWeight: 900
                }}>
                    You Win!
                </DialogTitle>
                <DialogContent>
                    <DialogContentText id="alert-dialog-description">
                        Congratulations on winning the game!
                    </DialogContentText>
                </DialogContent>
                <DialogActions>
                    <Button onClick={handleClose} variant="contained" style={{color: 'white'}}>Play again</Button>
                </DialogActions>
            </Dialog>
        </div>
    );
}


export const LoseDialog = ({ onOK, open }: PropsWonDialog) => {
    const handleClose = () => {
        onOK();
    };
    return (
        <div>
            <Dialog
                open={open}
                onClose={handleClose}
                aria-labelledby="alert-dialog-title"
                aria-describedby="alert-dialog-description"
            >
                <DialogTitle id="alert-dialog-title" style={{
                    color: red[600], fontWeight: 900
                }}>
                    You lose
                </DialogTitle>
                <DialogContent>
                    <DialogContentText id="alert-dialog-description">
                        Good luck next time in this game
                    </DialogContentText>
                </DialogContent>
                <DialogActions>
                    <Button onClick={handleClose} variant="contained" style={{color: 'white'}}>Play again</Button>
                </DialogActions>
            </Dialog>
        </div>
    );
}

type PropsErrorDialog = {
    onOK: () => void
    open: boolean
    message: string
}

export const ErrorDialog = ({ onOK, open, message }: PropsErrorDialog) => {
    const handleClose = () => {
        onOK();
    };
    return (
        <div>
            <Dialog
                open={open}
                onClose={handleClose}
                aria-labelledby="alert-dialog-title"
                aria-describedby="alert-dialog-description"
            >
                <DialogTitle id="alert-dialog-title" style={{
                    color: red[700], fontWeight: 900
                }}>
                    Error
                </DialogTitle>
                <DialogContent>
                    <DialogContentText id="alert-dialog-description">
                        {message}
                    </DialogContentText>
                </DialogContent>
                <DialogActions>
                    <Button onClick={handleClose} variant="contained" style={{color: 'white'}} color="error">OK</Button>
                </DialogActions>
            </Dialog>
        </div>
    );
}
