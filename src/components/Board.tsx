import React from 'react'
import { Box } from '@mui/material'
import { CurrentRow, EmptyRow, CompleteRow } from './BoardRow'


type PropsBoard = {
    words: string[],
    correctWord: string,
    maxRow?: number,
    currentRow: number,
    currentWord: string,
    presentLetters: string[]
}


export const Board = ({
    words, correctWord, maxRow, currentRow, currentWord, presentLetters
}: PropsBoard) => {
    const numberOfRow = Array.from(Array(maxRow || 6))
    return <Box>
        <Box
            sx={{
                display: 'flex',
                justifyContent: 'center',
                marginBottom: 1,
                flexDirection: 'column',
                padding: 4
            }}
        >
            {numberOfRow.map((_, index) => {
                    if (index < currentRow)
                        return <CompleteRow word={words[index]} correctWord={correctWord} key={index} presentLetters={presentLetters}/>
                    if (index === currentRow)
                        return <CurrentRow word={currentWord} correctWord={correctWord} key={index}/>
                    return <EmptyRow correctWord={correctWord} key={index}/>
                })
            }
        </Box>
    </Box>

}