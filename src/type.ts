export type CharStatus = 'absent' | 'present' | 'correct' | '' | undefined | null | 'normal'
