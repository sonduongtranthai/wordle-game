import React, { ReactElement, FC, useState, useEffect } from "react";
import Box from '@mui/material/Box';
import { Keyboard } from "../components/Keyboards";
import { Board } from '../components/Board'
import { ButtonGroup, IconButton, Tooltip } from '@mui/material'
import { RestartAlt, Settings } from '@mui/icons-material'
import { letterClassification, generateEmptyStringWithLength, checkValidWords } from '../utils/helper'
import * as api from '../utils/api'
import { ResetDialog, SettingDialog } from '../components/Dialog'
import { LoseDialog, WonDialog, ErrorDialog } from '../components/DialogAlert'


const Home: FC<any> = (): ReactElement => {
    const [characters, setLetters] = useState<string[]>([])
    const [words, setWords] = useState<string[]>([])
    const [correctWord, setCorrectWord] = useState<string>('_____')
    const [currentWord, setCurrentWord] = useState<string>('')
    const [correctLetters, setCorrectLetters] = useState<string[]>([])
    const [presentLetters, setPresentLetters] = useState<string[]>([])
    const [absentLetters, setAbsentLetters] = useState<string[]>([])
    const [currentRow, setCurrentRow] = useState<number>(0)
    const [maxRow, setMaxRow] = useState<number>(6)
    const [sizeWord, setSizeWord] = useState<number>(5)
    const [isGameWon, setIsGameWon] = useState<boolean>(false)
    const [isGameLose, setIsGameLose] = useState<boolean>(false)
    const [mode, setMode] = useState<string>('daily')
    const [seed, setSeed] = useState<number>(1234) // just use for random mode
    const [openSettingDialog, setOpenSettingDialog] = useState<boolean>(false)
    const [openResetDialog, setOpenResetDialog] = useState<boolean>(false)
    const [updateValue, setUpdateValue] = useState<any>()
    const [errorMessage, setErrorMessagee] = useState<string>('')

    useEffect(() => {
        if (updateValue) {
            reset()
            setMode(updateValue.mode || 'daily')
            setMaxRow(updateValue.maxRow || 6)
            setSizeWord(updateValue.sizeWord || 5)
            setCorrectWord(updateValue.correctWord || '')
            setOpenSettingDialog(false)
            // if mode is random, seed value will be generated
            if (updateValue.mode === 'daily' || updateValue.mode === 'random') {
                setCorrectWord(generateEmptyStringWithLength(updateValue.sizeWord || 5))
                setSeed(randomNumber())
            }
        }
    }, [updateValue])

    const randomNumber = () => {
        return Math.floor(Math.random() * 1000000);
    }

    const handleClickLetter = (value: string) => {
        setErrorMessagee('')
        if (!characters.includes(value))
            setLetters(current => [...current, value])
        if (currentWord.length < correctWord.length) {
            setCurrentWord(current => current += value)
        }
    }

    const handleClickEnter = () => {
        if (currentWord.length === correctWord.length && currentRow <= maxRow) {
            console.log('checkValidWords(currentWord)', checkValidWords(currentWord))
            if(!checkValidWords(currentWord)) {
                setErrorMessagee('Invalid word!')
                return
            }
            if (mode === 'daily')
                handleSubmitWordDaily(currentWord)
            if (mode === 'random')
                handleSubmitWordRandom(currentWord)
            if (mode === 'custom') {
                checkWin()
                goNextRow()
            }
        }
    }

    const checkWin = (response?: any[]) => {
        if (mode === 'custom') {
            if (currentWord.toUpperCase() === correctWord.toUpperCase())
                setIsGameWon(true)
            else
                if (currentRow + 1 === maxRow)
                    setIsGameLose(true)
        } else {
            if (checkCorrectFromAPI(response))
                setIsGameWon(true)
            else
                if (currentRow + 1 === maxRow)
                    setIsGameLose(true)
        }

    }

    const checkCorrectFromAPI = (res?: any[]): boolean => {
        if (!res) return false
        for (const item of res) {
            if (item.result === 'present' || item.result === 'absent')
                return false
        }
        return true
    }

    const goNextRow = () => {
        setWords(current => [...current, currentWord])
        setCurrentRow(current => current + 1)
        setCurrentWord('')
        handleAddLetters()
    }

    const handleResponse = (res: any) => {
        let customCorrectWord = ''
        for (const item of res) {
            if (correctWord[item.slot] !== '_') {
                customCorrectWord += correctWord[item.slot]
                continue
            }
            if (item.result === 'correct') {
                customCorrectWord += item.guess
                setCorrectLetters(current => [...current, ...item.guess.toUpperCase()])
                continue
            }
            if (item.result === 'present') {
                customCorrectWord += '_'
                setPresentLetters(current => [...current, ...item.guess.toUpperCase()])
                continue
            }
            customCorrectWord += '_'
            setAbsentLetters(current => [...current, ...item.guess.toUpperCase()])
        }
        setCorrectWord(customCorrectWord.toUpperCase())
        checkWin(res)
    }

    const handleSubmitWordDaily = (word: string) => {
        api.getGuessDaily(word, sizeWord).then(res => {
            handleResponse(res)
        }).catch((err: any) => {
            console.log('err', err)
        }).then(() => goNextRow())
    }

    const handleSubmitWordRandom = (word: string) => {
        api.getGuessRandom(word, sizeWord, seed).then(res => {
            handleResponse(res)
        }).catch((err: any) => {
            console.log('err', err)
        }).then(() => goNextRow())
    }

    const handleAddLetters = () => {
        const letters = letterClassification(currentWord, correctWord)
        setCorrectLetters(current => [...current, ...letters.correctLetters])
        setPresentLetters(current => [...current, ...letters.presentLetters])
        setAbsentLetters(current => [...current, ...letters.absentLetters])
    }

    const handleClickRemove = () => {
        if (currentWord) {
            const updateWords = currentWord.slice(0, -1)
            setCurrentWord(updateWords)
        }
    }

    const reset = () => {
        setCurrentRow(0)
        setCurrentWord('')
        setCorrectLetters([])
        setPresentLetters([])
        setAbsentLetters([])
        setWords([])
        setOpenResetDialog(false)
        setIsGameLose(false)
        setIsGameWon(false)
        setErrorMessagee('')
        if (mode === 'daily' || mode === 'random') {
            setCorrectWord(generateEmptyStringWithLength(sizeWord))
            setSeed(randomNumber())
        }
    }

    const handlePlayAgin = () => {
        reset()
        setIsGameWon(false)
        setIsGameLose(false)
        setOpenSettingDialog(true)
    }

    return (
        <Box
            sx={{
                display: 'flex',
                flexDirection: 'column',
                borderRadius: 1,
            }}
        >
            <ResetDialog
                onCancel={() => setOpenResetDialog(false)}
                open={openResetDialog}
                onReset={() => reset()}
            />
            <SettingDialog
                onCancel={() => setOpenSettingDialog(false)}
                onChangeSetting={setUpdateValue}
                open={openSettingDialog}
                currentSetting={{ mode, maxRow, correctWord }}
            />
            <LoseDialog open={isGameLose} onOK={() => handlePlayAgin()} />
            <WonDialog open={isGameWon} onOK={() => handlePlayAgin()} />
            <ErrorDialog open={errorMessage.length > 0} onOK={() => setErrorMessagee('')} message={errorMessage}/>

            <Box sx={{
                borderRadius: 1,
                position: 'absolute',
                right: 10
            }}>
                <ButtonGroup variant="outlined" aria-label="outlined button group">
                    <Tooltip title="Setting">
                        <IconButton onClick={() => setOpenSettingDialog(true)}>
                            <Settings />
                        </IconButton>
                    </Tooltip>
                    <Tooltip title="Reset">
                        <IconButton onClick={() => reset()}>
                            <RestartAlt />
                        </IconButton>
                    </Tooltip>
                </ButtonGroup>
            </Box>
            <Box> 
                <Board
                    currentRow={currentRow}
                    currentWord={currentWord}
                    words={words}
                    correctWord={correctWord}
                    maxRow={maxRow}
                    presentLetters={presentLetters}
                />
            </Box>
            <Box>
                <Keyboard onClick={handleClickLetter}
                    correctLetters={correctLetters}
                    presentLetters={presentLetters}
                    absentLetters={absentLetters}
                    onClickEnter={handleClickEnter}
                    onClickRemove={handleClickRemove}
                />
            </Box>
        </Box>
    );
};

export default Home;