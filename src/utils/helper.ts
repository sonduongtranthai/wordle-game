import EnglishWords1 from '../EnglishWord/english-words-10.json'
import EnglishWords2 from '../EnglishWord/english-words-20.json'
import EnglishWords3 from '../EnglishWord/english-words-35.json'
import EnglishWords4 from '../EnglishWord/english-words-40.json'
import EnglishWords5 from '../EnglishWord/english-words-50.json'
import EnglishWords55 from '../EnglishWord/english-words-55.json'
import EnglishWords6 from '../EnglishWord/english-words-60.json'
import EnglishWords7 from '../EnglishWord/english-words-70.json'

const letterClassification = (word: string, correctWord: string) => {
  const listCorrectLetter = correctWord.split('')
  const letters = word.split('')
  const correctLetters: string[] = []
  const absentLetters: string[] = []
  const presentLetters: string[] = []
  for (const key in listCorrectLetter) {
    if (letters[key] && (listCorrectLetter[key] === letters[key]) && !correctLetters.includes(letters[key]))
      correctLetters.push(letters[key].toUpperCase())
    else {
      if (letters[key] && listCorrectLetter.includes(letters[key]) && !correctLetters.includes(letters[key])) {
        presentLetters.push(letters[key].toUpperCase())
        continue
      }
      absentLetters.push(letters[key].toUpperCase())
    }
  }
  return { correctLetters, absentLetters, presentLetters }

}

const generateEmptyStringWithLength = (length: number) => {
  var result = '';
  for (var i = 0; i < length; i++) {
    result += '_'
  }
  return result;
}

const checkValidWords = (word: string) => {
  const lowerCaseWord = word.toLowerCase()
  if (EnglishWords1.includes(lowerCaseWord)
    || EnglishWords2.includes(lowerCaseWord)
    || EnglishWords3.includes(lowerCaseWord)
    || EnglishWords4.includes(lowerCaseWord)
    || EnglishWords5.includes(lowerCaseWord)
    || EnglishWords55.includes(lowerCaseWord)
    || EnglishWords6.includes(lowerCaseWord)
    || EnglishWords7.includes(lowerCaseWord)
  )
    return true
  return false

}

export { letterClassification, generateEmptyStringWithLength, checkValidWords }
