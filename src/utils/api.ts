import request from './request'

export const getGuessDaily = (guess: string, size?: number): Promise<any> => {
    const params = { guess, size: size || 5 }
    return request.get('/daily', { params })
}

export const getGuessRandom = (guess: string, size?: number, seed?: number): Promise<any> => {
    const params = { guess, size: size || 5 }
    if (seed) Object.assign(params, { seed })
    return request.get('/random', { params })
}

export const getGuessWord = (word: string, guess: string): Promise<any> => {
    const params = { guess, word }
    return request.get('/random', { params })
}
